#!/bin/sh
echo "###Entrypoint started work###"
if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "###PostgreSQL started###"
fi
echo "Clearing database...."
python manage.py flush --no-input  #Comment this command if you dont want clearing DB
echo "Start migrate....."
python manage.py migrate
echo "Create admin user...."
python manage.py create_admin

exec "$@"
