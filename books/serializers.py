from rest_framework import serializers

from .models import Book


class BookSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=True)
    title = serializers.CharField(min_length=5, max_length=255, required=True)
    publisher = serializers.CharField(max_length=255, required=True)
    author = serializers.CharField(max_length=255, required=True)
    pages = serializers.IntegerField(required=True)
    tags = serializers.ListField(allow_empty=True, required=True)

    class Meta:
        model = Book
        fields = (
            'id',
            'title',
            'publisher',
            'author',
            'pages',
            'tags',
            'created_at',
            'updated_at'
        )
