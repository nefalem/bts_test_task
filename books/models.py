from django.db import models
from django.contrib.postgres.fields import ArrayField


class Book(models.Model):
    id = models.IntegerField(primary_key=True, unique=True)
    title = models.CharField(max_length=255, blank=False)
    publisher = models.CharField(max_length=255, blank=False)
    author = models.CharField(max_length=255, blank=False)
    pages = models.IntegerField(blank=False)
    tags = ArrayField(models.CharField(max_length=200), blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
