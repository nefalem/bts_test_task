import logging

from rest_framework import status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import BookSerializer
from .models import Book
logger = logging.getLogger('books')


class BookBaseAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request:any, title:str):
        """Обработка GET запроса"""
        book = get_object_or_404(Book, title=title)
        serializer = BookSerializer(book)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        """Обработка POST запроса"""
        data = request.data
        serializer = BookSerializer(data=data)

        if serializer.is_valid(raise_exception=True):

            if Book.objects.filter(title=request.data['title']).exists():
                return Response(
                    {'success': 'False', 'Error': f'Book with title: {data["title"]} is already exits'},
                    status=status.HTTP_400_BAD_REQUEST
                )
            if Book.objects.filter(id=request.data['id']).exists():
                return Response(
                    {'success': 'False', 'Error': f'Book with id: {data["id"]} is already exits'},
                    status=status.HTTP_400_BAD_REQUEST
                )

            serializer.save()
            return Response({'success': 'True', 'created_book': serializer.data}, status=status.HTTP_201_CREATED)

    def put(self, request):
        """Обработка PUT запроса"""
        data = request.data
        serializer = BookSerializer(data=data)

        if serializer.is_valid(raise_exception=True):
            created_book = Book.objects.update_or_create(
                id=data['id'],
                defaults={
                    'title': data['title'],
                    'publisher': data['publisher'],
                    'author': data['author'],
                    'pages': data['pages'],
                    'tags': data['tags'],
                }

            )
            logger.info(created_book)
            return Response(
                {'success': 'True', 'changed_book': serializer.data},
                status=status.HTTP_201_CREATED
            )

    def delete(self, request, title):
        """Обработка DELETE запроса"""
        created_book = get_object_or_404(Book.objects.all(), title=title)
        created_book.delete()
        return Response(
            {'success': 'True', 'status': f'Book with title: {title} successfully deleted'},
            status=status.HTTP_204_NO_CONTENT
        )
