from .views import BookBaseAPIView
from django.urls import path

urlpatterns = [

    path('books/<str:title>/', BookBaseAPIView.as_view()),
    path('books/', BookBaseAPIView.as_view()),

]
