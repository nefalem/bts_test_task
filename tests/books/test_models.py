# app/tests/books/test_models.py
import pytest
from books.models import Book


@pytest.mark.django_db
def test_books_model():
    book = Book(
        id=1,
        title="The Hitchhiker’s Guide to the Galaxy",
        publisher="Del Rey",
        author="Douglas Noël Adams",
        pages=815,
        tags=["Science fiction", "Humor", "Novels"]
    )
    book.save()
    assert book.title == "The Hitchhiker’s Guide to the Galaxy"
    assert book.publisher == "Del Rey"
    assert book.author == "Douglas Noël Adams"
    assert book.pages == 815
    assert book.tags == ["Science fiction", "Humor", "Novels"]
    assert book.created_at
    assert book.updated_at
