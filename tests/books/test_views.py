import pytest

from books.models import Book


@pytest.mark.django_db
def test_create_book(client, create_jwt_token):
    response = client.post(
        "/api/v1/books/",
        {
            "id": 1,
            "title": "Test book",
            "publisher": "Test publisher",
            "author": "Test author",
            "pages": 999,
            "tags": ["fantasy", "test book", "horrors"]
        },
        HTTP_AUTHORIZATION=create_jwt_token,
        content_type="application/json"
    )

    assert response.status_code == 201
    assert response.data["created_book"]['title'] == "Test book"
    assert response.data["created_book"]['publisher'] == "Test publisher"


@pytest.mark.django_db
def test_create_book_without_authorization(client):
    response = client.post(
        "/api/v1/books/",
        {
            "id": 1,
            "title": "Test book",
            "publisher": "Test publisher",
            "author": "Test author",
            "pages": 999,
            "tags": ["fantasy", "test book", "horrors"]
        },
        content_type="application/json"
    )
    assert response.status_code == 401


@pytest.mark.django_db
def test_create_book_without_json_fields(client, create_jwt_token):
    response = client.post(
        "/api/v1/books/",
        HTTP_AUTHORIZATION=create_jwt_token,
        content_type="application/json"
    )
    assert response.status_code == 400


@pytest.mark.django_db
def test_get_book_by_title(client, create_jwt_token):
    book = Book.objects.create(
        id=1,
        title="Test book",
        publisher="Test publisher",
        author="Test author",
        pages=999,
        tags=["fantasy", "test book", "horrors"]
    )
    resp = client.get(
        f"/api/v1/books/{book.title}/",
        HTTP_AUTHORIZATION=create_jwt_token,
        content_type="application/json"
    )
    assert resp.status_code == 200
    assert resp.data["title"] == "Test book"


@pytest.mark.django_db
def test_get_book_by_incorrect_title(client, create_jwt_token):

    book = Book.objects.create(
        id=1,
        title="Test book",
        publisher="Test publisher",
        author="Test author",
        pages=999,
        tags=["fantasy", "test book", "horrors"]
    )
    resp = client.get(
        "/api/v1/books/Ricardo Milos/",
        HTTP_AUTHORIZATION=create_jwt_token,
        content_type="application/json"
    )
    assert resp.status_code == 404


@pytest.mark.django_db
def test_update_exists_book(client, create_jwt_token):
    book = Book.objects.create(
        id=1,
        title="Test book",
        publisher="Test publisher",
        author="Test author",
        pages=999,
        tags=["fantasy", "test book", "horrors"]
    )
    resp = client.put(
        "/api/v1/books/",
        {
            "id": 1,
            "title": "Test new book",
            "publisher": "Test new publisher",
            "author": "Test author",
            "pages": 777,
            "tags": ["humor", "test book", "horrors"]
        },
        HTTP_AUTHORIZATION=create_jwt_token,
        content_type="application/json"
    )
    assert resp.status_code == 201
    assert resp.data["changed_book"]["title"] == "Test new book"
    assert resp.data["changed_book"]["publisher"] == "Test new publisher"
    assert resp.data["changed_book"]["pages"] == 777
    assert resp.data["changed_book"]["tags"] == ["humor", "test book", "horrors"]


@pytest.mark.django_db
def test_delete_book_by_title(client, create_jwt_token):
    book = Book.objects.create(
        id=1,
        title="Test book",
        publisher="Test publisher",
        author="Test author",
        pages=999,
        tags=["fantasy", "test book", "horrors"]
    )
    resp = client.delete(
        f"/api/v1/books/{book.title}/",
        HTTP_AUTHORIZATION=create_jwt_token,
        content_type="application/json"
    )
    assert resp.status_code == 204
