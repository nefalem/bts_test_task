from books.serializers import BookSerializer


def test_valid_book_serializer():

    valid_serializer_data = {
        "id": 1,
        "title": "Test book",
        "publisher": "Test publisher",
        "author": "Test author",
        "pages": 999,
        "tags": ["fantasy", "test book", "horrors"]
    }

    serializer = BookSerializer(data=valid_serializer_data)

    assert serializer.is_valid()
    assert serializer.validated_data == valid_serializer_data
    assert serializer.data == valid_serializer_data
    assert serializer.errors == {}


def test_invalid_book_serializer():

    invalid_serializer_data = {
        "id": 1,
        "publisher": "Test publisher",
        "author": "Test author",
        "pages": 999,
        "tags": ["fantasy", "test book", "horrors"]
    }

    serializer = BookSerializer(data=invalid_serializer_data)

    assert not serializer.is_valid()
    assert serializer.validated_data == {}
    assert serializer.data == invalid_serializer_data
    assert serializer.errors == {"title": ["This field is required."]}
