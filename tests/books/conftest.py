import os
import pytest

from django.conf import settings
from django.contrib.auth.models import User
from rest_framework_simplejwt.tokens import RefreshToken


DEFAULT_ENGINE = "django.db.backends.postgresql_psycopg2"


@pytest.fixture(scope="session")
def django_db_setup():
    settings.DATABASES["default"] = {
        "ENGINE": os.environ.get("SQL_ENGINE", DEFAULT_ENGINE),
        "HOST": os.environ["SQL_HOST"],
        "NAME": os.environ["DB_TEST_NAME"],
        "PORT": os.environ["SQL_PORT"],
        "USER": os.environ["SQL_USER"],
        "PASSWORD": os.environ["SQL_PASSWORD"],
    }


@pytest.fixture
def create_jwt_token() -> str:
    user = User.objects.create_user(
        username='test_user',
        password='86MrjHv@?YB*Zhf)',
        email='test@test.com'
    )
    refresh = RefreshToken.for_user(user)
    return 'JWT ' + str(refresh.access_token)

